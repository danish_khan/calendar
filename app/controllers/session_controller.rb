class SessionController < ApplicationController
  def new
  end
  def create
    user = User.find_by_email(params[:session][:email].downcase)

    if User.user_exists(params[:session][:email].downcase) && User.authenticate(params[:session][:email].downcase,params[:session][:password]) == 'true'
      log_in user
      Rails.logger.error user
      Rails.logger.error logged_in?
      redirect_to "/"
    else
      flash[:danger] = 'Invalid email/password combination'
      render 'new'
    end
  end
  def destroy
    log_out
    redirect_to '/'
  end
end
