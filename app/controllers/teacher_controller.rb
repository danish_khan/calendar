class TeacherController < ApplicationController
  def addUser
    email = params[:signupemail].downcase
    password = params[:signuppassword]
    if (!User.valid_email?(email) || !User.valid_password?(password))
      flash[:error] = 'Bad email!'
      redirect_to '/signup'
    else
      recaptcha_response = params['g-recaptcha-response']
      school_service_url = 'http://52.11.254.225:8080/users/addUser'
      urlObj=RestHandler.new ({
                                 "url"=>school_service_url,
                                 "method"=>"GET",
                                 "requestObj"=>{
                                     "email"=>email,
                                     "password"=>password
                                 }
                             })
      if verify_recaptcha()
        response=urlObj.sendCall
        if response.code == "200"
          redirect()
        else
          redirect_to :action => 'signup'
        end
      else
        redirect_to :action => 'signup'
      end
    end

  end

  def redirect
    google_api_client = Google::APIClient.new({
                                                  application_name: 'Example Ruby application',
                                                  application_version: '1.0.0'
                                              })

    google_api_client.authorization = Signet::OAuth2::Client.new({
                                                                     client_id: ENV.fetch('GOOGLE_API_CLIENT_ID'),
                                                                     client_secret: ENV.fetch('GOOGLE_API_CLIENT_SECRET'),
                                                                     authorization_uri: 'https://accounts.google.com/o/oauth2/auth',
                                                                     scope: 'https://www.googleapis.com/auth/calendar',
                                                                     redirect_uri: url_for(:action => :callback)
                                                                 })

    authorization_uri = google_api_client.authorization.authorization_uri

    redirect_to authorization_uri.to_s
  end

  def callback
    google_api_client = Google::APIClient.new({
                                                  application_name: 'Example Ruby application',
                                                  application_version: '1.0.0'
                                              })

    google_api_client.authorization = Signet::OAuth2::Client.new({
                                                                     client_id: ENV.fetch('GOOGLE_API_CLIENT_ID'),
                                                                     client_secret: ENV.fetch('GOOGLE_API_CLIENT_SECRET'),
                                                                     token_credential_uri: 'https://accounts.google.com/o/oauth2/token',
                                                                     redirect_uri: url_for(:action => :callback),
                                                                     code: params[:code]
                                                                 })

    response = google_api_client.authorization.fetch_access_token!

    session[:access_token] = response['access_token']

    redirect_to url_for(:action => :calendars)
  end

  def calendars
    google_api_client = Google::APIClient.new({
                                                  application_name: 'Example Ruby application',
                                                  application_version: '1.0.0'
                                              })

    google_api_client.authorization = Signet::OAuth2::Client.new({
                                                                     client_id: ENV.fetch('GOOGLE_API_CLIENT_ID'),
                                                                     client_secret: ENV.fetch('GOOGLE_API_CLIENT_SECRET'),
                                                                     access_token: session[:access_token]
                                                                 })

    google_calendar_api = google_api_client.discovered_api('calendar', 'v3')

    response = google_api_client.execute({
                                             api_method: google_calendar_api.calendar_list.list,
                                             parameters: {}
                                         })

    @items = response.data['items']
  end
end
